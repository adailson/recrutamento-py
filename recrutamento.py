import tkinter as tk
import sqlite3
from tkinter import filedialog
import base64

pdf_base64 = None  # Variável global para armazenar temporariamente o PDF em base64

def armazenar_pdf():
    global pdf_base64
    file_path = filedialog.askopenfilename()
    with open(file_path, "rb") as file:
        pdf_content = file.read()
        pdf_base64 = base64.b64encode(pdf_content).decode('utf-8')
        print("PDF armazenado com sucesso!")

def cadastrar_candidato():
    nome = entry_nome.get()
    idade = entry_idade.get()
    cidade = entry_cidade.get()
    estado = entry_estado.get()
    telefone = entry_telefone.get()
    email = entry_email.get()

    # Inserir os dados no banco de dados
    conexao = sqlite3.connect('candidatos.db')
    cursor = conexao.cursor()

    cursor.execute('CREATE TABLE IF NOT EXISTS candidatos (id INTEGER PRIMARY KEY, nome TEXT, idade INTEGER, cidade TEXT, estado TEXT, telefone TEXT, email TEXT, curriculo TEXT)')

    cursor.execute('INSERT INTO candidatos (nome, idade, cidade, estado, telefone, email, curriculo) VALUES (?, ?, ?, ?, ?, ?, ?)',
                   (nome, idade, cidade, estado, telefone, email, pdf_base64))

    conexao.commit()
    conexao.close()

    print("Candidato cadastrado com sucesso!")

def remover_candidato(candidato_id):
    conexao = sqlite3.connect('candidatos.db')
    cursor = conexao.cursor()

    cursor.execute('DELETE FROM candidatos WHERE id = ?', (candidato_id,))
    conexao.commit()
    conexao.close()

    print(f"Candidato {candidato_id} removido com sucesso!")
    listar_candidatos()  # Atualizar a lista após a remoção

def listar_candidatos():
    ver_candidatos_window = tk.Toplevel(root)
    ver_candidatos_window.title("Candidatos Cadastrados")

    conexao = sqlite3.connect('candidatos.db')
    cursor = conexao.cursor()

    cursor.execute('SELECT * FROM candidatos')
    candidatos = cursor.fetchall()

    for candidato in candidatos:
        info = f"ID: {candidato[0]} - Nome: {candidato[1]}\nIdade: {candidato[2]}\nCidade: {candidato[3]}\nEstado: {candidato[4]}\nTelefone: {candidato[5]}\nEmail: {candidato[6]}"
        tk.Label(ver_candidatos_window, text=info).pack()

        # Botão para remover o candidato
        button_remover = tk.Button(ver_candidatos_window, text="Remover", command=lambda id=candidato[0]: remover_candidato(id))
        button_remover.pack()

    conexao.close()

root = tk.Tk()
root.title("Cadastro de Candidato")

# Criar os campos para inserção dos dados
label_nome = tk.Label(root, text="Nome:")
label_nome.pack()
entry_nome = tk.Entry(root)
entry_nome.pack()

label_idade = tk.Label(root, text="Idade:")
label_idade.pack()
entry_idade = tk.Entry(root)
entry_idade.pack()

label_cidade = tk.Label(root, text="Cidade:")
label_cidade.pack()
entry_cidade = tk.Entry(root)
entry_cidade.pack()

label_estado = tk.Label(root, text="Estado:")
label_estado.pack()
entry_estado = tk.Entry(root)
entry_estado.pack()

label_telefone = tk.Label(root, text="Telefone:")
label_telefone.pack()
entry_telefone = tk.Entry(root)
entry_telefone.pack()

label_email = tk.Label(root, text="E-mail:")
label_email.pack()
entry_email = tk.Entry(root)
entry_email.pack()

button_curriculo = tk.Button(root, text="Inserir curriculo", command=armazenar_pdf)
button_curriculo.pack()

button_curriculo = tk.Button(root, text="Cadastrar candidato", command=cadastrar_candidato)
button_curriculo.pack()

button_ver_candidatos = tk.Button(root, text="Ver Candidatos Cadastrados", command=listar_candidatos)
button_ver_candidatos.pack()

root.mainloop()
